import os
import pickle

from dotenv import load_dotenv
from hydra import compose, initialize
from hydra.utils import instantiate
from omegaconf import DictConfig
from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split


def train(cfg: DictConfig):
    iris = load_iris()
    x = iris["data"]
    y = iris["target"]

    x_train, _, y_train, _ = train_test_split(x, y, test_size=0.2)

    preprocessor = instantiate(cfg.preprocess)
    x_train_preprocessed = preprocessor.fit_transform(x_train)

    model = instantiate(cfg.model)
    model.fit(x_train_preprocessed, y_train)

    model_dir = os.path.join(os.getcwd(), "models")
    os.makedirs(model_dir, exist_ok=True)
    model_path = os.path.join(model_dir, "trained_model.pkl")
    pickle.dump(model, open(model_path, "wb"))


if __name__ == "__main__":
    load_dotenv()

    initialize(version_base=None, config_path="../conf")
    config = compose(config_name="config.yaml")
    train(config)
