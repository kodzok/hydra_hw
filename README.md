# Repo for ODS Hydra HW

1. Clone repository:
    ```bash
    git clone git@gitlab.com:kodzok/hydra_hw.git
    ```
2. Set up the project:
    ```bash
   cd hydra_hw
   export PYTHONPATH=./
   conda env create -f environment.yaml -y
   ```
3. Run `train.py`:
    ```bash
   python src/train.py
   ```
   or
    ```bash
   python src/train preprocess=min_max_scaler
   ```
   or some other options from `conf`
